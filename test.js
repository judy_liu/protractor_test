
describe('Vevo Test', function() {
    
    function signIn(emailKey, passwordKey) {
        // Clicks sign-in link
        element(by.linkText('Sign In')).click();
        browser.waitForAngular();
        
        // Expect to be on the sign-in page
        var signInHeader = element.all(by.className('heading')).first().getText();
        expect(signInHeader).toEqual("SIGN IN."); 
        
        
        // Expect to be sign in successfully
        var email = element(by.name("inputEmail"));
        var password = element(by.name("inputPassword"));
        var submitBtn = element(by.buttonText("SIGN IN"));
                
        email.sendKeys(emailKey);
        password.sendKeys(passwordKey);
        
        submitBtn.click();
        browser.waitForAngular();
    }

    it('should have title', function() {
        browser.get('http://www.vevo.com/');
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual('Vevo | New Music Videos, Premieres, Concerts & Original Shows');
    });
    
    
    it('should sign in with correct user displayed', function() {
        signIn("judy.liu.16@gmail.com", "testvevo");
        
        // Check for user name
        expect(element(by.binding("userName")).getText()).toEqual("Judy");
        
        // Check for user status
        expect($('[ng-show="userLoggedIn"').isDisplayed()).toBeTruthy(); 
    });
    
    
    it('should sign out', function() {
        element(by.binding("userName")).click();
        browser.waitForAngular();
        
        element(by.linkText('SIGN OUT')).click();
        browser.waitForAngular();
        
        // Check user status
        expect($('[ng-show="!userLoggedIn"').isDisplayed()).toBeTruthy();
    });
  
  
    it('should not sign in', function() {
        signIn("judy.liu.16@gmail.com", "wrongpassword");
        
        // Check login status
        expect($('[ng-show="errorCode == 404"').isDisplayed()).toBeTruthy();
    });
    
    
});
